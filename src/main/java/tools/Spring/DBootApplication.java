package tools.Spring;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class DBootApplication implements ApplicationRunner{

	@Autowired
	JdbcTemplate jdbc;
	
	@Value("${sql:}")
	String sql;
	
	@Value("${delimiter:\t}")
	String delimiter;
	
	public static void main(String[] args) {
		SpringApplication.run(DBootApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments argList) throws Exception {
		List<Map<String, Object>> results = jdbc.queryForList(sql);
		results.get(0).keySet().forEach(key -> System.out.print(key + delimiter));
		System.out.println();
		results.forEach(result -> {
			result.keySet().forEach(column -> System.out.print(result.get(column) + delimiter));
			System.out.println();
		});
	}
}
