# DBoot
Application to query any type of database without external drivers installed

This is meant to be a personal tools since I don't have DB2 installed on my 
workstation at my internship and this little program is more fun than the 
installation process. Also saves me from the craziness of the local 
configuration problems that come up on my local machine when I'm just needing 
to try a simple thing.

NOTE:
There's almost no validation on this application. This gives a huge concern in
any deployable component of an application or script.
